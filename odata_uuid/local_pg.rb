DB_NAME = 'drone-area-registry-db'

# Start Local postgres DB server for developing
#
def start_local_pg(dir: "#{__dir__}/db/", port: 6432, host: 'localhost')
  db_url = "postgres://user:user@#{host}:#{port}/database_name"
  env = "-e POSTGRES_DB=database_name -e POSTGRES_USER='user' -e POSTGRES_PASSWORD='user' -e PGDATA='/var/lib/postgresql/data/pgdata'"

  at_exit do;  system "docker stop #{DB_NAME}" end

  Thread.new do
    system %{mkdir -p #{dir} && cd #{dir} && docker run -p #{port}:5432 --rm --name #{DB_NAME} \
    #{env} -v "$(pwd)/data:/var/lib/postgresql/data" postgres:13.5 > /dev/null 2>&1 }
  end

  Sequel.connect(db_url).disconnect \
    rescue (puts 'waiting for db connection'; sleep 3; retry)
db_url
end

module Sequel
  module Postgres
    module EnumDatabaseMethods
      def create_enum?(enum, values)
        create_enum(enum, values) unless from(:pg_type).where(typname: enum.to_s).count > 0
      end
    end
  end
end
