require 'logger'
require 'safrano'
require_relative 'local_pg.rb'

DB = Sequel.connect ENV['DB_URL'] || start_local_pg
DB.extension :connection_validator, :pg_enum
DB.loggers << Logger.new($stdout)
DB.sql_log_level = :debug

Sequel::Model.db = DB

DB.create_enum? :state, %w'open limited closed'

DB.execute 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp"'

# DB.drop_table? :entities

DB.create_table? :entities do
  uuid :id, primary_key: true, default: Sequel.function(:uuid_generate_v4)
  foreign_key :parent_id, type: 'uuid' #, :entities
  state  :state
  DateTime :created_at
  DateTime :updated_at
  String :name, text: true
  String :email, text: true
end

class Entity < Sequel::Model
  # plugin :uuid
  plugin :timestamps, create: :created_at, update: :updated_on, update_on_create: true
  many_to_one :parent, class: self
  one_to_many :children, key: :parent_id, class: self

  plugin :association_dependencies, children: :destroy # children: :delete
end

class ODataSrv < Safrano::ServerApp
  publish_service do
    publish_model Entity, :entities do
      add_nav_prop_single :parent
      add_nav_prop_collection :children
    end
  end
end

run ODataSrv.new
