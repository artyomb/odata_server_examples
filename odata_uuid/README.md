# OData REST API
Exaxmples:
```shell
http://localhost:7000/entities/$count
http://localhost:7000/entities/?$top=10&$skip=10
http://localhost:7000/entities/?$orderby=id
http://localhost:7000/entities?$select=id,name,created_at&$filter=type%20eq%20%27plane%27
http://localhost:7000/entities/entities(2)/parent
http://localhost:7000/entities(2)/children
http://localhost:7000/entities(guid'4554f31a-7382-43eb-bfb4-b93ab4a0de51')
http://localhost:7000/entities(guid'4554f31a-7382-43eb-bfb4-b93ab4a0de51')/?$expand=children,parent
```
Response: 
```json
{
  "d": {
    "__metadata": { "uri": "http://localhost:9494/entities(4554f31a-7382-43eb-bfb4-b93ab4a0de51)", "type": "Entity" },
    "id": "4554f31a-7382-43eb-bfb4-b93ab4a0de51",
    "parent_id": null,
    "name": null,
    "email": null,
    "parent": { "__deferred": { "uri": "http://localhost:9494/entities(4554f31a-7382-43eb-bfb4-b93ab4a0de51)/parent" }},
    "children": {"__deferred": { "uri": "http://localhost:9494/entities(4554f31a-7382-43eb-bfb4-b93ab4a0de51)/children"}}
  }
}
```

# Post example
```shell
 curl http://localhost:7000/entities -H 'Content-Type: application/json' -d '{"name":"value"}'
 ```


# Metadata (swagger analog)
Read more [OData - the best way to REST](https://www.odata.org/)
```shell
http://localhost:7000/$metadata
```
```xml
<edmx:Edmx xmlns:edmx="http://schemas.microsoft.com/ado/2007/06/edmx" Version="1.0">
<edmx:DataServices xmlns:m="http://schemas.microsoft.com/ado/2007/08/dataservices/metadata" m:DataServiceVersion="2.0">
<Schema xmlns="http://schemas.microsoft.com/ado/2009/11/edm">
<EntityType Name="Entity">
<Key>
<PropertyRef Name="id"/>
</Key>
<Property Name="id" Type="Edm.Guid" Nullable="false"/>
<Property Name="parent_id" Type="Edm.Guid"/>
<Property Name="state"/>
<Property Name="created_at" Type="Edm.DateTime" Precision="0"/>
<Property Name="updated_at" Type="Edm.DateTime" Precision="0"/>
<Property Name="name" Type="Edm.String"/>
<Property Name="email" Type="Edm.String"/>
<NavigationProperty Name="parent" Relationship=".Entity_Entity" FromRole="Entity" ToRole="Entity"/>
<NavigationProperty Name="children" Relationship=".Entity_Entity" FromRole="Entity" ToRole="Entity"/>
</EntityType>
<Association Name="Entity_Entity">
<End Type=".Entity" Role="Entity" Multiplicity="*"/>
<End Type=".Entity" Role="Entity" Multiplicity="*"/>
</Association>
<EntityContainer m:IsDefaultEntityContainer="true">
<EntitySet Name="entities" EntityType="Entity"/>
<AssociationSet Name="Entity_Entity" Association=".Entity_Entity">
<End EntitySet="entities" Role="Entity"/>
<End EntitySet="entities" Role="Entity"/>
</AssociationSet>
</EntityContainer>
</Schema>
</edmx:DataServices>
</edmx:Edmx>

```