FROM ruby:3.1.2-alpine
RUN apk add --update build-base libpq-dev openssl-dev docker-cli && rm -rf /var/cache/apk/*

WORKDIR /app
ADD Gemfile*  /app/

RUN echo 'gem: --no-document' >> ~/.gemrc
RUN bundle install --jobs $(nproc)

ADD *.ru *.rb  /app/

ENV RACK_ENV=production
ENV PORT=7000

CMD bundle exec rackup -p $PORT

# docker run --rm -it --network host -v /var/run/docker.sock:/var/run/docker.sock $(docker build -q .)
