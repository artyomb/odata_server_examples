# OData server
This is a collection of examples of an OData server implemented using the [**safrano**](https://gitlab.com/dm0da/safrano/) gem.
These are fully self-contained, finished examples ready for launch and extend.

Read more at [OData - the best way to REST](https://www.odata.org/)

[[_TOC_]]

## OData UUID
[/odata_uuid](odata_uuid)

The OData UUID example illustrates how to set up a table with UUID type IDs in PostgreSQL.
Subsequently, you can access the resource in the following manner:
```ruby
http://localhost:7000/entities(guid'4554f31a-7382-43eb-bfb4-b93ab4a0de51')
http://localhost:7000/entities(guid'4554f31a-7382-43eb-bfb4-b93ab4a0de51')/?$expand=children,parent
```

